/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
// import Home from './src/Tugas1/screens/Home'
// import Home from './src/Tugas2/screens/Home'
// import Home from './src/Tugas3/screens/Home'
import Home from './src/Tugas4/screens'

const App: () => React$Node = () => {
  return (
    <>
      <Home />
      {/* <Home /> */}
      {/* <TodoList /> */}
      {/* <Context /> */}
    </>
  );
};

export default App;
