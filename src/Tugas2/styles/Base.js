import {StyleSheet} from 'react-native'

const styling = StyleSheet.create({
    bgGreen: {
        backgroundColor: '#cee397'
    },
    photoUser: {
        width: 60,
        height: 60,
        borderRadius: 40,
        marginRight: 16
    }, 
    bgBlue: {
        backgroundColor: "#3797a4",
        padding: 10,
    },
    title: {
        fontSize: 24,
        fontWeight: 'bold',
    },
    flexRow: {
        flexDirection: 'row',
        alignItems: 'center',

    },
    itemDesc: {
        justifyContent: 'space-between',
        marginHorizontal: 8,
        paddingHorizontal: 8
    },
    userName: {
        padding: 16
    }
})

export default styling