import React from 'react'
import {View, Text, Image} from 'react-native'
import styling from '../styles/Base'

const User = () => {
    return(
        <>
        <View style={{paddingBottom: 10}}>
            <Text style={[styling.bgBlue, styling.title, {color: '#fff', fontWeight: '500', padding: 16, fontSize: 20}]}>Account</Text>
            <View style={[styling.flexRow, styling.userName]}>
                <Image 
                    style={styling.photoUser}
                    source={require('../assets/img/fotoSaya.jpg')} 
                />
                <Text style={[styling.title, {fontSize:16}]}>M Taufik Baskoro</Text>
            </View>
        </View>
        </>
    )
}

export default User