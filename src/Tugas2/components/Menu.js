import React from 'react'
import {View, Text} from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome5';
import styling from '../styles/Base'

const Menu = ({icon, desc}) => {
    return(
        <>
            <View>
                <View style={[styling.flexRow, {paddingHorizontal: 16, paddingVertical: 10}]}>
                    <View>
                        <Icon name={icon} size={30} color="#8bcdcd" />
                    </View>
                    <View style={[styling.flexRow, styling.itemDesc, {flexGrow: 1}]}>
                        <Text>{desc}</Text>
                    </View>
                </View>
            </View>
        </>
    )
}

export default Menu