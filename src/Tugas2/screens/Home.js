import React from 'react'
import { StatusBar, Text, View } from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome5';
import Menu from '../components/Menu'
import User from '../components/User'
import styling from '../styles/Base'

const Home = () => {
    return(
        <>
            <View>
                <StatusBar barStyle="light-content" backgroundColor="#3797a4"></StatusBar>
                <User />
                <View style={[styling.flexRow, {paddingHorizontal: 16, paddingVertical: 10}]}>
                    <View>
                        <Icon name="wallet" size={30} color="#8bcdcd" />
                    </View>
                    <View style={[styling.flexRow, styling.itemDesc, {flexGrow: 1}]}>
                        <Text>Saldo</Text>
                        <Text>Rp. 40.000</Text>
                    </View>
                </View>
                <View style={{marginTop: 16}}>
                    <Menu icon="tools" desc="Pengaturan" />
                    <Menu icon="question-circle" desc="Bantuan" />
                    <Menu icon="file-alt" desc="Syarat dan Ketentuan" />
                </View>
                <View style={[styling.flexRow, {paddingHorizontal: 16, paddingVertical: 10, marginTop: 16}]}>
                    <View>
                        <Icon name="sign-out-alt" size={30} color="#8bcdcd" />
                    </View>
                    <View style={[styling.flexRow, styling.itemDesc, {flexGrow: 1}]}>
                        <Text>Keluar</Text>
                    </View>
                </View>
            </View>
        </>
    )
}

export default Home;