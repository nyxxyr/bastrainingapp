import React from 'react'
import { StatusBar, Text, View } from 'react-native'

const Home = () => {
    return(
        <View style={{
            flex:1,
            justifyContent: 'center',
            alignItems: 'center'
        }}>
            <StatusBar barStyle="dark-content" backgroundColor="#fff"></StatusBar>
            <Text>Hello Kelas React Native Lanjutan Sanbercode!</Text>
        </View>
    )
}

export default Home