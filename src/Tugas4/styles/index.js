import {StyleSheet} from 'react-native'

const styles = StyleSheet.create({
    container: {
        backgroundColor: '#9ad3bc',
        padding: 16,
    },
    title: {
        fontSize: 16,
        fontWeight: 'bold',
        color: '#f3eac2'
    },
    activity: {
        fontSize: 14,
        color: '#333'
    },
    subActivity: {
        fontSize: 12,
        color: '#333'
    },
    btnAdd: {
        backgroundColor: '#f5b461', 
        justifyContent: 'center',
        borderRadius: 4,
        padding: 16
    },
    btnDelete: {
        backgroundColor: '#ec524b', 
        justifyContent: 'center',
        borderRadius: 4,
        padding: 16
    },
    inputTodo: {
        width: 300,
        padding: 16,
        borderStyle: 'solid',
        borderWidth: 1,
        borderColor: '#f5b461',
        borderRadius: 4,
        color: '#f5b461',
    },
    inputContainer: {
        margin: 16,
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    itemList: {
       backgroundColor: '#9ad3bc',
       margin: 16,
       borderRadius: 4,
       maxHeight: 500 
    },
    item: {
        backgroundColor: '#fff',
        margin: 16,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        borderRadius: 4
    }
})

export default styles