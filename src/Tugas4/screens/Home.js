import React, { useContext } from 'react'
import {View, Text, ScrollView, StatusBar, TextInput, TouchableOpacity, FlatList} from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome5';
import Item from '../components/item'
import { RootContext } from '../api/Context'
import styles from '../styles'

const TodoList = () => {
    const state = useContext(RootContext)
    
    const renderTodo = ({item}) => {
        return(
            <Item item={item} />
        )
    }

    return(
        <>
            <View>
                <StatusBar barStyle="light-content" backgroundColor="#9ad3bc"></StatusBar>
                <View style={styles.container}>
                    <Text style={styles.title}>Masukkan TodoList</Text>
                </View>
                <View style={styles.inputContainer}>
                    <TextInput 
                        placeholder="Tambah Kegiatan"
                        placeholderTextColor="#f5b461"
                        selectionColor="#f5b461"
                        style={styles.inputTodo}
                        value={state.input}
                        onChangeText={(value) => {state.handleInput(value)}}
                    />
                    <TouchableOpacity 
                        style={styles.btnAdd}
                        onPress={state.handleBtnAdd}
                        >
                        <Icon name="plus" size={24} color="#fff" />
                    </TouchableOpacity>
                </View>
                <ScrollView 
                    scrollEnabled={true}
                    style={styles.itemList}>
                    <FlatList 
                        data={state.todos}
                        renderItem={renderTodo}
                    />  
                </ScrollView>
            </View>
        </>
    )
}



export default TodoList
