import React, { useContext } from 'react'
import {View, Text, TouchableOpacity} from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome5';
import { RootContext } from '../api/Context';
import styles from '../styles'

const Item = ({item}) => {
    const state = useContext(RootContext)

    return(
        <View key={item.id} style={styles.item}>
            <View style={{marginLeft: 8}}>
                <Text style={styles.subActivity}>{item.date}</Text>
                <Text style={styles.activity}>{item.desc}</Text>
            </View>
            <TouchableOpacity 
                style={styles.btnDelete}
                onPress={() => {state.handleBtnRemove(item.id)}}>
                <Icon name="trash" size={24} color="#fff" />
            </TouchableOpacity>
        </View>
    )
}

export default Item