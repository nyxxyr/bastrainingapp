import React, { createContext, useState } from 'react'
import TodoList from '../screens/Home'

export const RootContext = createContext()

const Context = () => {
    const [input, setInput] = useState('')
    const [todos, setTodos] = useState([])

    const handleInput = (val) => {
        setInput(val)
    }

    const handleBtnAdd = () => {
        if(input.length > 0){
            let date = new Date().getDate()
            let month = new Date().getMonth()
            let year = new Date().getFullYear()
            let datetime = `${date}/${month}/${year}`
            setTodos([...todos, {id: Math.random(), date: datetime, desc: input}])
            setInput('')
        }
    }

    const handleBtnRemove = (res) => {
        let temp = todos
        temp = temp.filter((todo) => {
            return todo.id !== res
        })
        setTodos(temp)
    }

    return(
        <RootContext.Provider value={{
            input,
            todos,
            handleInput,
            handleBtnAdd,
            handleBtnRemove
        }}>
            <TodoList />
        </RootContext.Provider>
    )
}

export default Context
