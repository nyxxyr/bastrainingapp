import React from 'react'
import {View, Text, TouchableOpacity} from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome5';
import styles from '../styles'

const Item = ({detail, index, deleteTodo}) => {
    return(
        <View key={index} style={styles.item}>
            <View style={{marginLeft: 8}}>
                <Text style={styles.subActivity}>{detail.date}</Text>
                <Text style={styles.activity}>{detail.desc}</Text>
            </View>
            <TouchableOpacity 
                style={styles.btnDelete}
                onPress={() => {deleteTodo(detail.id)}}>
                <Icon name="trash" size={24} color="#fff" />
            </TouchableOpacity>
        </View>
    )
}

export default Item
