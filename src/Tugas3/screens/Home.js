import React, { useState } from 'react'
import {View, Text, ScrollView, StatusBar, TextInput, TouchableOpacity} from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome5';
import styles from '../styles'
import Item from '../components/Item'

const TodoList = () => {
    const [input, setInput] = useState('')
    const [todos, setTodos] = useState([])

    const addTodo = () => {
        let temp = [...todos]
        let tanggal = new Date()
        temp.push({
            id: Math.random(),
            date: tanggal.toDateString(),
            desc: input
        })
        setTodos(temp)
        setInput('')
    }

    const deleteTodo = (e) => {
        let temp = [...todos]
        temp = temp.filter(todo => {
            return todo.id !== e
        })
        setTodos([...temp])
    }

    return(
        <>
            <ScrollView>
                <StatusBar barStyle="light-content" backgroundColor="#9ad3bc"></StatusBar>
                <View style={styles.container}>
                    <Text style={styles.title}>Masukkan TodoList</Text>
                </View>
                <View style={styles.inputContainer}>
                    <TextInput 
                        placeholder="Tambah Kegiatan"
                        placeholderTextColor="#f5b461"
                        selectionColor="#f5b461"
                        style={styles.inputTodo}
                        onChangeText={(e) => setInput(e)}
                        value={input}
                    />
                    <TouchableOpacity 
                        style={styles.btnAdd}
                        onPress={addTodo}>
                        <Icon name="plus" size={24} color="#fff" />
                    </TouchableOpacity>
                </View>
                <ScrollView style={styles.itemList}>
                    {
                        todos.length > 0 ? (
                        <>{todos.map((todo, index) => {
                            return(
                                <Item detail={todo} index={index} deleteTodo={deleteTodo} />
                            )
                        })}</>
                        ) : (<></>)
                    }
                </ScrollView>
            </ScrollView>
        </>
    )
}

export default TodoList;